package hibernate.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import hibernate.dao.PostDaoImpl;
import hibernate.dao.PostsEntity;
import hibernate.model.PostResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
public class MainRESTController {

    @Autowired
    private PostDaoImpl postDao;
    @Autowired
    private ObjectMapper mapper;

    @RequestMapping("/welcome")
    @ResponseBody
    public String welcome() {
        return "Welcom to RestTemplate Example";
    }

    // URL:
    // http://localhost:8080/login/post
    // http://localhost:8080/login/post.xml
    // http://localhost:8080/login/post.json

    @RequestMapping(value = "/post",
            method = RequestMethod.GET,
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public @ResponseBody PostResponse findAllPost() {
        return new PostResponse(postDao.findAllPost());
    }
}
