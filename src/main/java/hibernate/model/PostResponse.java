package hibernate.model;

import hibernate.dao.PostsEntity;

import java.util.List;

public class PostResponse {
    private final List<PostsEntity> entities;

    public PostResponse(List<PostsEntity> entities) {
        this.entities = entities;
    }

    public List<PostsEntity> getEntities() {
        return entities;
    }
}
