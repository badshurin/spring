package hibernate.main;

import hibernate.dao.PostsEntity;
import org.hibernate.Session;
import hibernate.utils.HibernateSessionFactory;

public class AppMain {

    public static void main(String[] args) {
        System.out.println("Hibernate tutorial");
        {
            Session session = HibernateSessionFactory.getSessionFactory().openSession();

            session.beginTransaction();

            PostsEntity postEntity = new PostsEntity();

            postEntity.setName("Hiber3");
            postEntity.setPassword("tutu3");

            session.save(postEntity);
            session.getTransaction().commit();

            session.close();
        }
        {
            Session session2 = HibernateSessionFactory.getSessionFactory().openSession();

            session2.beginTransaction();

            PostsEntity postEntity2 = new PostsEntity();

            postEntity2.setId(1);
            postEntity2.setName("Hiber4");
            postEntity2.setPassword("tutu4");

            session2.save(postEntity2);
            session2.getTransaction().commit();

            session2.close();
        }


    }
}