package hibernate.servlets;

import hibernate.services.AccountService;
import hibernate.services.AccountServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet(name = "AddPost", urlPatterns = "/add")
public class AddPost extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        AccountService accountService = new AccountServiceImpl();
        switch (accountService.testData(name, password)){
            case 1:
                response.sendRedirect("/newuser?name=" + name);
                break;
            case 0:
                request.getRequestDispatcher("WEB-INF/password.jsp").forward(request,response);
                break;
            case -1:
                response.sendRedirect("/uexist?name=" + name);
                break;
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
