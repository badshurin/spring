package hibernate.servlets;

import hibernate.services.AccountService;
import hibernate.services.AccountServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ComeIn", urlPatterns = "/comein")
public class ComeIn extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String password = request.getParameter("password");
        AccountService accountService = new AccountServiceImpl();
        if (accountService.netPost(name,password)) {
          response.sendRedirect("/backuser?name=" +name);
      }
      else {
          response.sendRedirect("/nnp");
      }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
