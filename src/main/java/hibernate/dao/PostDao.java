package hibernate.dao;

import org.hibernate.service.spi.ServiceException;

import java.util.List;

public interface PostDao {
    PostsEntity byName(String name) throws ServiceException;
    void addPost(PostsEntity postsEntity);
    List<PostsEntity> findAllPost();
}
