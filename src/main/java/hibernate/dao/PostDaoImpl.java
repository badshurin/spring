package hibernate.dao;

import hibernate.utils.HibernateSessionFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.hibernate.service.spi.ServiceException;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PostDaoImpl implements PostDao {
    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory){
        this.sessionFactory=sessionFactory;
    }

    @Override
    public PostsEntity byName(String name) throws ServiceException {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        Query query = session.createQuery("from PostsEntity where name = :paramName");
        query.setParameter("paramName", name);
        List list = query.list();
        PostsEntity post = (PostsEntity) list.get(0);
        if (session.isOpen()){
            session.close();
        }
        return post;
    }

    @Override
    public void addPost(PostsEntity user) {
        try{
            Session session = HibernateSessionFactory.getSessionFactory().openSession();
            Transaction tx1 = session.beginTransaction();
            session.save(user);
            tx1.commit();
            if (session.isOpen()){
               session.close();
            }
        }
        catch (ServiceException e){
            System.out.println(e.fillInStackTrace());
        }
    }

    @Override
    public List<PostsEntity> findAllPost() {
        Session session = HibernateSessionFactory.getSessionFactory().openSession();
        List<PostsEntity> user = session.createQuery("from PostsEntity").list();
        if (session.isOpen()){
            session.close();
        }
        return user;
    }
}
